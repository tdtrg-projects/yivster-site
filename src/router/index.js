import TheNavigation from '../components/TheNavigation.vue';

const routes = [
	{
		path: '/home',
		component: TheNavigation,
	},
];

const router = new VueRouter({
	routes // short for `routes: routes`
  })

export default router;