import Vue from 'vue';
import Vuex from 'vuex';
import InputSearch from '../elements/InputSearch'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules:{
    InputSearch
  }
});