import Vue from 'vue'
import App from './App.vue'

/**
 * bootstrap
 */
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

/**
 * font awesomes
 */
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faSearch)
library.add(faAngleDown)

Vue.component('font-awesome-icon', FontAwesomeIcon)

/**
 * Vue router
 */
import VueRouter from 'vue-router'
Vue.use(VueRouter)
//test
/**
 * vuex
 */
import store from '../src/components/store'

/**
 * routing configuration
 */
import GuestHome from './components/pages/GuestHome';
import LoginPage from './components/pages/LoginPage';
import RegisterPage from './components/pages/RegisterPage';
import SearchVideos from './components/pages/SearchVideos';
import VideoDetail from './components/pages/VideoDetail';
import VideoUpload from './components/pages/VideoUpload';
import UserDashboard from './components/pages/UserDashboard';
import UserPlayList from './components/pages/UserPlayList';
import UserPlaylistDetail from './components/pages/UserPlaylistDetail';
import NotFound from './components/NotFound';

const routes = [
  { 
    path: '/',
    component: GuestHome 
  },
  { 
    path: '/login',
    component: LoginPage 
  },
  { 
    path: '/register',
    component: RegisterPage 
  },
  { 
    path: '/dashboard',
    component: UserDashboard 
  },
  { 
    path: '/playlist',
    component: UserPlayList 
  },
  { 
    path: '/playlists/:id',
    component: UserPlaylistDetail 
  },
  {
    path: '/search/:plateform',
    component:SearchVideos
  },
  {
    path:'/video/:plateform',
    component:VideoDetail
  },
  {
    path:'/videos/upload',
    component:VideoUpload
  },
  {
    path: '/404', name: 'NotFound', component: NotFound
  },
  {
    path: '/:catchAll(.*)', redirect:'404'
  }
]

const router = new VueRouter({
  routes,
  mode: 'history' 
})
new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
